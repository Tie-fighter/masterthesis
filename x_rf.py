import logging

import numpy as np
import pandas
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import (accuracy_score, f1_score, precision_score,
                             recall_score)

import my_config as mc
import wandb

labels = dict()


def main():
    log = logging.getLogger(__name__)

    # disable output truncating
    #pandas.set_option('display.max_rows', None)

    trainset = pandas.DataFrame()
    log.info("Reading: %smy_3_train.csv", mc.workingdir)
    # , dtype = dtypes)
    trainset = pandas.read_csv("%smy_3_train.csv", mc.workingdir)
    # infer best possible dtypes
    trainset = trainset.convert_dtypes()

    testset = pandas.DataFrame()
    log.info("Reading: %smy_3_test.csv", mc.workingdir)
    # , dtype = dtypes)
    testset = pandas.read_csv("%smy_3_test.csv", mc.workingdir)
    testset = testset.convert_dtypes()

    X_train = trainset.loc[:, trainset.columns != 'Label']
    y_train = trainset.loc[:, trainset.columns == 'Label']

    y_train = np.ravel(y_train)
    y_train = y_train.astype('int')

    X_test = testset.loc[:, trainset.columns != 'Label']
    y_test = testset.loc[:, trainset.columns == 'Label']

    y_test = np.ravel(y_test)
    y_test = y_test.astype('int')

    # Sweep setup
    config_defaults = dict(
        n_estimators=100,
        criterion="gini",
        max_depth=None,
        min_samples_split=2,
        min_samples_leaf=1.0,
        min_weight_fraction_leaf=0.0,
        max_features="auto",
        max_leaf_nodes=None,
        min_impurity_decrease=0.0
    )

    log.info("Initalising wandb")
    run = wandb.init(project="framed", config=config_defaults, project="randomforest")

    # Setup classifier
    log.info("Creating classifier...")
    rf = RandomForestClassifier(verbose=1, n_jobs=-1,
                                n_estimators=run.config.n_estimators,
                                criterion=run.config.criterion,
                                max_depth=run.config.max_depth,
                                min_samples_split=run.config.min_samples_split,
                                min_samples_leaf=run.config.min_samples_leaf,
                                min_weight_fraction_leaf=run.config.min_weight_fraction_leaf,
                                max_features=run.config.max_features,
                                max_leaf_nodes=run.config.max_leaf_nodes,
                                min_impurity_decrease=run.config.min_impurity_decrease,
                                )

    # Train with train set
    log.info("Fitting model...")
    rf.fit(X_train, y_train)  # (X,y)

    # Test against test set
    log.info("Predicting test dataset...")
    y_pred = rf.predict(X_test)  # (X)

    # Calculate scores
    accuracy = accuracy_score(y_test, y_pred)
    log.info("Accuracy: %s", accuracy)
    #precision = precision_score(y_test, y_predicted)
    #log.info("Precision: " + str(precision))
    #recall = recall_score(y_test, y_predicted)
    #log.info("Recall: " + str(recall))
    #f1 = f1_score(y_test, y_predicted, average='micro')
    #log.info("F1 score: " + str(f1))

    # Log result to WandB
    wandb.log({"accuracy": accuracy})
    wandb.sklearn.plot_confusion_matrix(y_test, y_pred)

    # titles_options = [("Confusion matrix, without normalization", None),
    #                ("Normalized confusion matrix", 'true')]
    # for title, normalize in titles_options:
    #    disp = plot_confusion_matrix(rf, X_test, y_test,
    #                                 display_labels=class_names,
    #                                 cmap=plt.cm.Blues,
    #                                 normalize=normalize)
    # disp.ax_.set_title(title)

    # print(title)
    # print(disp.confusion_matrix)


if __name__ == "__main__":
    import sys
    main()
