import tensorflow as tf
import wandb
from wandb.keras import WandbCallback

from keras.datasets import fashion_mnist
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, Dropout, Dense, Flatten
from keras.utils import np_utils
from keras.optimizers import SGD
from keras.optimizers import RMSprop, SGD, Adam, Nadam
from keras.callbacks import ReduceLROnPlateau, ModelCheckpoint, Callback, EarlyStopping

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)


(X_train, y_train), (X_test, y_test) = fashion_mnist.load_data()
labels = ["T-shirt/top", "Trouser", "Pullover", "Dress", "Coat",
          "Sandal", "Shirt", "Sneaker", "Bag", "Ankle boot"]

img_width = 28
img_height = 28

X_train = X_train.astype('float32') / 255.
X_test = X_test.astype('float32') / 255.

# reshape input data
X_train = X_train.reshape(X_train.shape[0], img_width, img_height, 1)
X_test = X_test.reshape(X_test.shape[0], img_width, img_height, 1)

# one hot encode outputs
y_train = np_utils.to_categorical(y_train)
y_test = np_utils.to_categorical(y_test)
num_classes = y_test.shape[1]

sweep_config = {
    'method': 'random',  # grid, random
    'metric': {
        'name': 'accuracy',
        'goal': 'maximize'
    },
    'parameters': {
        'epochs': {
            'values': [2, 5, 10]
        },
        'batch_size': {
            'values': [256, 128, 64, 32]
        },
        'dropout': {
            'values': [0.3, 0.4, 0.5]
        },
        'conv_layer_size': {
            'values': [16, 32, 64]
        },
        'weight_decay': {
            'values': [0.0005, 0.005, 0.05]
        },
        'learning_rate': {
            'values': [1e-2, 1e-3, 1e-4, 3e-4, 3e-5, 1e-5]
        },
        'optimizer': {
            'values': ['adam', 'nadam', 'sgd', 'rmsprop']
        },
        'activation': {
            'values': ['relu', 'elu', 'selu', 'softmax']
        }
    }
}


sweep_id = wandb.sweep(
    sweep_config, entity="tie-fighter", project="sweep_test")

# The sweep calls this function with each set of hyperparameters


def train():
    # Default values for hyper-parameters we're going to sweep over
    config_defaults = {
        'epochs': 5,
        'batch_size': 128,
        'weight_decay': 0.0005,
        'learning_rate': 1e-3,
        'activation': 'relu',
        'optimizer': 'nadam',
        'hidden_layer_size': 128,
        'conv_layer_size': 16,
        'dropout': 0.5,
        'momentum': 0.9,
        'seed': 42
    }

    # Initialize a new wandb run
    wandb.init(project="framed", config=config_defaults)

    # Config is a variable that holds and saves hyperparameters and inputs
    config = wandb.config

    # Define the model architecture - This is a simplified version of the VGG19 architecture
    model = Sequential()

    # Set of Conv2D, Conv2D, MaxPooling2D layers with 32 and 64 filters
    model.add(Conv2D(filters=config.conv_layer_size, kernel_size=(3, 3), padding='same',
                     activation='relu', input_shape=(img_width, img_height, 1)))
    model.add(Dropout(config.dropout))

    model.add(Conv2D(filters=config.conv_layer_size, kernel_size=(3, 3),
                     padding='same', activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Flatten())
    model.add(Dense(config.hidden_layer_size, activation='relu'))

    model.add(Dense(num_classes, activation="softmax"))

    # Define the optimizer
    if config.optimizer == 'sgd':
        optimizer = SGD(lr=config.learning_rate, decay=1e-5,
                        momentum=config.momentum, nesterov=True)
    elif config.optimizer == 'rmsprop':
        optimizer = RMSprop(lr=config.learning_rate, decay=1e-5)
    elif config.optimizer == 'adam':
        optimizer = Adam(lr=config.learning_rate, beta_1=0.9,
                         beta_2=0.999, clipnorm=1.0)
    elif config.optimizer == 'nadam':
        optimizer = Nadam(lr=config.learning_rate, beta_1=0.9,
                          beta_2=0.999, clipnorm=1.0)

    model.compile(loss="categorical_crossentropy",
                  optimizer=optimizer, metrics=['accuracy'])

    model.fit(X_train, y_train, batch_size=config.batch_size,
              epochs=config.epochs,
              validation_data=(X_test, y_test),
              callbacks=[WandbCallback(data_type="image", validation_data=(X_test, y_test), labels=labels),
                         EarlyStopping(patience=10, restore_best_weights=True)])


wandb.agent(sweep_id, train)
