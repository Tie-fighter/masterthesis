import argparse
import logging
import os
import random
import sys

import pandas as pd
from sklearn.model_selection import train_test_split

import wandb

parser = argparse.ArgumentParser()
parser.add_argument('--dataset', type=str, required=True,
                    help='the wandb dataset to be cleaned up (e.g. IDS2018-cleaned:latest)')
parser.add_argument('--testsize', type=float, required=True, help='size of the testset (e.g. 0.2)')
parser.add_argument('--seed', type=int, required=False, help='seed for seeding the PRNGs')

log = logging.getLogger(__name__)

def main(argv):

    args = parser.parse_args()

    run = wandb.init(project="framed", job_type='split_dataset:%s' % args.dataset, tags=["preprocessing"])
    run.config.update(args)

    # Seed PRNGs
    if not run.config.seed:
        run.config.update(dict(seed=random.randrange(2**16)), allow_val_change=True)

    log.info("Using PRNG seed: %s", run.config.seed)
    os.environ['PYTHONHASHSEED'] = str(run.config.seed)
    random.seed(run.config.seed)

    artifact = run.use_artifact(args.dataset)
    artifact_dir = artifact.download()

    for r, d, f in os.walk(artifact_dir):
        for file in f:
            if '.csv' in file:
                fullfilepath = os.path.join(artifact_dir, file)

    data = pd.DataFrame()
    log.info("Reading: %s", fullfilepath)
    data = pd.read_csv(fullfilepath)

    trainset = pd.DataFrame()
    testset = pd.DataFrame()

    # split the data into training and test part
    trainset, testset = train_test_split(data, test_size=args.testsize, stratify=data["Label"])

    # write resulting csv
    file = file.replace("-cleaned.csv", "-train.csv")
    log.debug(trainset.info())
    log.info("Writing: %s", file)
    trainset.to_csv(file, index=False)
    log.info("Creating wandb artifact: %s", file.replace(".csv", ""))
    artifact = wandb.Artifact(file.replace(".csv", ""), type='train-dataset', metadata=dict(run.config))
    artifact.add_file(file)
    run.log_artifact(artifact)

    file = file.replace("-train.csv", "-test.csv")
    log.debug(testset.info())
    log.info("Writing: %s",  file)
    testset.to_csv(file, index=False)
    log.info("Creating wandb artifact: %s", file.replace(".csv", ""))
    artifact = wandb.Artifact(file.replace(".csv", ""), type='test-dataset', metadata=dict(run.config))
    artifact.add_file(file)
    run.log_artifact(artifact)


if __name__ == "__main__":
    main(sys.argv)
