import argparse
import logging
import os
import sys
import random
import math


import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import progressbar
import tabulate
import plotly.express as px

import my_config as mc
import my_functions as mf
import wandb

# register commandline parameters
parser = argparse.ArgumentParser(description='Analyse a wandb dataset')
parser.add_argument('--dataset', type=str, required=True,
                    help='the wandb dataset to be analysed (e.g. IDS2018-raw:latest)')

log = logging.getLogger(__name__)


def main(argv):

    args = parser.parse_args()

    # disable output truncating
    pd.set_option('display.max_rows', None)

    # start wandb run
    run = wandb.init(project="framed", job_type='analyse_dataset:%s' %
                     args.dataset, tags=["preprocessing"])
    # update run.config so the cl parameters show up in wandb
    run.config.update(args)

    # pull the specified dataset from wandb
    artifact = run.use_artifact(args.dataset)
    artifact_dir = artifact.download()

    dataset = pd.DataFrame()
    # discover all .csv-files of the downloaded dataset
    for r, d, f in os.walk(artifact_dir):
        for file in f:
            if '.csv' in file:
                fullfilepath = os.path.join(artifact_dir, file)
                log.info("Reading: %s", fullfilepath)
                dataset = dataset.append(pd.read_csv(fullfilepath))

    dataset.info(verbose=True)

    mf.logstats(dataset)

    print("Analysing data:")
    mybar = progressbar.ProgressBar(maxval=len(
        dataset.columns), redirect_stdout=True, term_width=150)
    progress = 0
    mybar.start()

    table = []
    metadata = dict()

    for col in dataset.columns:
        if pd.api.types.is_numeric_dtype(dataset.dtypes[col]):
            # calculate merged data values
            metadata["distinct."+ col] = dataset[col].nunique()
            metadata["na." + col] = dataset[col].isna().sum()
            metadata["min." + col] = dataset[col].min()
            metadata["max." + col] = dataset[col].max()
            metadata["median." + col] = dataset[col].median()
            metadata["mean." + col] = dataset[col].mean()

            table.append([col, metadata["distinct." + col], metadata["na." + col], metadata["min." + col], metadata["max." + col], metadata["median." + col], metadata["mean." + col]])
            log.debug(col + "\t" + str(dataset.dtypes[col]) + "\t" + str(metadata["distinct." + col]) + "\t" + str(metadata["min." + col]) + "\t" + str(metadata["max." + col]) + "\t" + str(metadata["median." + col]) + "\t" + str(
                metadata["mean." + col]) + "\t" + str(dataset.dtypes[col]))

            if np.isfinite(metadata["min." + col]) and np.isfinite(metadata["max." + col]):
                fig = px.histogram(dataset[col], nbins=100, log_y=True, marginal="rug")
                wandb.log({col: fig})
            else:
                print("Cannot plot (values not finite): " + col)

        else:
            print(col + " is of type " + str(dataset.dtypes[col]))
        progress = progress+1
        mybar.update(progress)

    mybar.finish()

    wandb.log(metadata)

    print(tabulate.tabulate(table, headers=["Name", "Distinct values", "Missing", "Min", "Max",
                                            "Median", "Mean"]))

    log.info("Calculating correlations...")
    correlation = dataset.corr()
    plt.figure(num="Correlation of features")
    plt.tight_layout()
    mask = np.triu(np.ones_like(correlation, dtype=np.bool))
    sns.heatmap(correlation, mask=mask, xticklabels=True,
                yticklabels=True, cmap="vlag", linewidths=.75)
    plt.show()


if __name__ == "__main__":
    main(sys.argv)
