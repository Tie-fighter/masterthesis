import logging

import pandas as pd

import my_config as mc
import my_functions as mf
import wandb


def main():
    log = logging.getLogger(__name__)                      # instantiate logger
    run = wandb.init(project="framed", job_type="acquire_datasets", tags=["preprocessing"])          # start wandb run
    pd.set_option('display.max_rows', None)                # disable output truncating

    # discover all .csv-files of the datasets specified in the config.ini
    files = dict()
    for d in mc.datasets:
        files[d] = mf.discoverfiles(mc.datasets.get(d))
    log.info("Reading files...")

    # read the .csv-files and merge them into one .csv per dataset
    for dataset in files:
        data = pd.DataFrame()
        for f in files.get(dataset):
            log.info("Reading %s", f[1])
            # skip rows if there an entry in the config.ini (only IDS2018)
            if f[1] in mc.skiprows:
                log.debug("skipping rows: %s", mc.skiprows[f[1]])
                newdata = pd.read_csv(f[0], skiprows=mc.skiprows[f[1]])
            else:
                log.debug("not skipping any rows...")
                newdata = pd.read_csv(f[0])
                # use the filename as the label (only myflows)
                label = f[1].split(".")
                newdata['Label'] = label[0]

            log.debug("Data shape: %s, size: %s elements", newdata.shape, newdata.size)
            log.debug("First 5 rows:")
            log.debug(newdata.head())
            data = data.append(newdata)

        # print some metadata of the dataset
        log.debug("Datatypes:")
        log.debug(data.dtypes)
        log.debug("Data shape: %s, size: %s elements", data.shape, data.size)
        log.info(data.info(verbose=True))
        mf.logstats(data)

        # write the merged dataset in a <datasetname>-raw.csv
        log.info("Writing: %s-raw.csv", dataset)
        data.to_csv(dataset + "-raw.csv", index=False)

        # push the <datasetname>-raw.csv to wandb artifact versioning
        log.info("Creating wandb artifact: %s", dataset + "-raw")
        artifact = wandb.Artifact(dataset+"-raw", type='raw-dataset')
        artifact.add_file(dataset + "-raw.csv")
        run.log_artifact(artifact)


if __name__ == "__main__":
    main()
