import logging

import numpy as np
import pandas
from tpot import TPOTClassifier

import my_config as mc

labels = dict()


def main():
    log = logging.getLogger(__name__)

    # disable output truncating
    #pandas.set_option('display.max_rows', None)

    trainset = pandas.DataFrame()
    log.info("Reading: %smy_3_train.csv", mc.workingdir)
    trainset = pandas.read_csv("%smy_3_train.csv", mc.workingdir)
    # infer best possible dtypes
    #trainset = trainset.convert_dtypes()

    testset = pandas.DataFrame()
    log.info("Reading: %smy_3_test.csv", mc.workingdir)
    testset = pandas.read_csv("%smy_3_test.csv", mc.workingdir)
    # infer best possible dtypes
    #testset = testset.convert_dtypes()

    X_train = trainset.loc[:, trainset.columns != 'Label']
    y_train = trainset.loc[:, trainset.columns == 'Label']

    y_train = np.ravel(y_train)
    y_train = y_train.astype('int')

    X_test = testset.loc[:, trainset.columns != 'Label']
    y_test = testset.loc[:, trainset.columns == 'Label']

    y_test = np.ravel(y_test)
    y_test = y_test.astype('int')

    cpfolder = mc.workingdir + "tpot_pipelines\\"
    # print(cpfolder)
    tpot = TPOTClassifier(generations=100, population_size=100, random_state=42, verbosity=2,
                          early_stop=5, n_jobs=1, memory='auto', periodic_checkpoint_folder=cpfolder)
    log.info("Starting tpot.fit ...")
    tpot.fit(X_train, y_train)
    log.info(tpot.score(X_test, y_test))
    tpot.export('tpot_pipeline.py')


if __name__ == "__main__":
    main()
