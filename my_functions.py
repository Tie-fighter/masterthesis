# required for walking the filesystem
import os
import time
import tensorflow as tf
import wandb
import pandas as pd
import numpy as np

# required for logging
import my_logger

# set log name
log = my_logger.logging.getLogger(__name__)

# crawls the pcapdir (incl. subdirectories) for .csv files
def discoverfiles(datasetdir):
    files = []
    log.info("Looking for files in %s", datasetdir)
    for r, d, f in os.walk(datasetdir):
        for file in f:
            if '.csv' in file:
                files.append([os.path.join(r, file), file])
    for f in files:
        log.debug("Found: %s", f[1])
    return files

# log some statistics about the dataset
def logstats(dataset):
    # number of all columns except "Label"
    log.info("Features: %s", len(dataset.loc[:, dataset.columns != 'Label'].columns))
    # number of unique values in column "Label"
    log.info("Classes: %s", dataset["Label"].nunique())
    log.info("Label frequencies:")
    log.info(dataset["Label"].value_counts())
    log.info("Label relative frequencies:")
    log.info(dataset["Label"].value_counts(normalize=True))


def load_datasets(run, datasets, seed):
    dataset = pd.DataFrame()
    for s in datasets:
        artifact = run.use_artifact(s)
        artifact_dir = artifact.download()

        for r, d, f in os.walk(artifact_dir):
            for file in f:
                if '.csv' in file:
                    fullfilepath = os.path.join(artifact_dir, file)
        log.info("Reading: %s", fullfilepath)
        dataset = dataset.append(pd.read_csv(fullfilepath))
    
    dataset = dataset.sample(frac=1, random_state=seed)

    x_dataset = dataset.loc[:, dataset.columns != 'Label']
    y_dataset = dataset.loc[:, dataset.columns == 'Label']

    y_dataset = np.ravel(y_dataset)
    y_dataset = y_dataset.astype('int')

    return x_dataset, y_dataset


class TimeHistory(tf.keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.times = []

    def on_epoch_begin(self, batch, logs={}):
        self.epoch_time_start = time.time()

    def on_epoch_end(self, batch, logs={}):
        epoch_time = (time.time() - self.epoch_time_start)
        self.times.append(epoch_time)
        wandb.log({'epoch_time': epoch_time})
