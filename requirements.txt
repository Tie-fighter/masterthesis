# Basic
configparser==5.0.0

# Data Preprocessing
pandas==1.0.4
numpy==1.19.5
seaborn==0.10.1
ppscore==0.0.2

# Nice Outputs
tabulate
progressbar2

# Machinelearning
sklearn==0.0
tpot==0.11.5
xgboost==1.3.3

# Neural Networks
tensorflow==2.4.1
keras==2.4.3
autokeras==1.0.2

wandb==0.10.25
graphviz==0.8.4