# ast is used for parsing in getskiprows()
import ast
# configparser is used for parsing the ini-file
import configparser
# np for understanding the datatypes in the config file
import numpy as np


import my_logger

log = my_logger.logging.getLogger(__name__)


# Configuration
config = configparser.ConfigParser()
config.optionxform = lambda option: option
config.read('config.ini')

log.debug("Reading config:")
for section in config:
    log.debug(section)
    log.debug(config.items(section))

# set dataset directories
datasets = dict()
for dataset in config["DATASETS"].items():
    datasets[dataset[0]] = dataset[1]
log.debug("datasets: %s", datasets)

# set labels
labels = dict()
for item in config.items("LABELS"):
    labels[item[0]] = int(item[1])
log.debug("Labels: ")
log.debug(labels)

# generate a dictionary of classnames
# uses the labels dict with swapped keys and values
classnames = {value: key for key, value in labels.items()}
classnames.pop(-2)  # remove lables without number

# parses the configured rows to be skipped
skiprows = dict()
for item in config.items("SKIPROWS"):
    my_list = ast.literal_eval(item[1])
    skiprows[item[0]] = my_list
log.debug("skiprows: ")
log.debug(skiprows)

# parse the column dtypes
columns = dict()
for item in config.items("DTYPES"):
    columns[item[0]] = item[1]

log.debug("columns: ")
log.debug(columns)

# parse the dtypes
dtypes = dict()
for key, value in columns.items():
    if value not in ('drop', 'auto'):
        dtypes[key] = eval(value)

log.debug("dtypes: ")
log.debug(dtypes)
