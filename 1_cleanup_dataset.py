import argparse
import logging
import os
import sys

import pandas

import my_config as mc
import wandb

# register commandline parameters
parser = argparse.ArgumentParser(description='Cleanup a wandb dataset')
parser.add_argument('--dataset', type=str, required=True,
                    help='the wandb dataset to be cleaned up (e.g. IDS2018-raw:latest)')

log = logging.getLogger(__name__)


def main(argv):

    args = parser.parse_args()

    # start wandb run
    run = wandb.init(project="framed", job_type='cleanup_dataset:%s' % args.dataset, tags=["preprocessing"])
    # update run.config so the cl parameters show up in wandb
    run.config.update(args)

    # pull the specified dataset from wandb
    artifact = run.use_artifact(args.dataset)
    artifact_dir = artifact.download()

    # discover all .csv-files of the downloaded dataset
    for r, d, f in os.walk(artifact_dir):
        for file in f:
            if '.csv' in file:
                fullfilepath = os.path.join(artifact_dir, file)

                # Start the cleanup
                #
                # 0_acquire_datasets should have turned "NaN" into ",," and "Infinity" into "inf"
                # Here we turn the ",," and "inf" into "0" using converters
                data = pandas.DataFrame()
                log.info("Reading with converters: %s", fullfilepath)
                # Convert and drop useless columns
                data = pandas.read_csv(fullfilepath,
                                       usecols=lambda x: mc.columns[x] != "drop",
                                       converters={"Label": convert_label_to_integer,
                                                   "Flow Byts/s": convert_infinity_or_nan,
                                                   "Flow Pkts/s": convert_infinity_or_nan,
                                                   "Src Port": convert_infinity_or_nan,
                                                   },
                                       )

                # Drop unwanted rows (labels that have a key of -2)
                log.info("Dropping rows with unwanted labels...")
                rowsbefore = len(data)
                data = data[data.Label != -2]
                log.info("Dropped %i rows.", rowsbefore - len(data))

                # Remove duplicates
                log.info("Dropping duplicates...")
                dedup = len(data)
                data.drop_duplicates(inplace=True)
                log.info("Dropped %i duplicates.", dedup - len(data))

                # look for missing values (there shouldn't be any)
                log.info("Looking for missing values (there should be non):")
                log.info(data.isna().sum())
                log.info(data.info())

                # write the cleaned dataset as <datasetname>-cleaned.csv
                file = file.replace("-raw.csv", "-cleaned.csv")
                log.info("Writing: %s", file)
                data.to_csv(file, index=False)

                # push the <datasetname>-raw.csv to wandb artifact versioning
                log.info("Creating wandb artifact: %s", file.replace(".csv",""))
                artifact = wandb.Artifact(file.replace(".csv", ""), type='cleaned-dataset')
                artifact.add_file(file)
                run.log_artifact(artifact)


def convert_infinity_or_nan(value):
    value = value.lower()
    if value == "inf" or value == "nan" or value == "":
        #    if np.isinf(value) or np.isnan(value) or value == "":
        return 0
    else:
        return value


def convert_label_to_integer(value):
    value = str.lower(value)
    if value in mc.labels:
        return mc.labels[value]
    else:
        log.error(value + " not found in config")
        return -1


if __name__ == "__main__":
    main(sys.argv)
