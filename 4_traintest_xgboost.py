import argparse
import logging
import os
import random
import sys

import numpy as np
import sklearn
import sklearn.ensemble
import sklearn.metrics
import sklearn.tree
import xgboost
from joblib import dump

import my_functions as mf
import my_config as mc
import wandb

parser = argparse.ArgumentParser()
parser.add_argument('--trainsets', type=str, required=True,
                    help='the wandb datasets used for training, comma-separated (e.g. IDS2018-train:latest,myflows-train:latest)')
parser.add_argument('--testsets', type=str, required=True,
                    help='the wandb datasets used for testing, comma-separated  (e.g. IDS2018-test:latest,myflows-test:latest)')
parser.add_argument('--base-estimator', type=str, required=False,
                    default="DecisionTreeClassifier(max_depth=1)")
parser.add_argument('--max_depth', type=int, required=False, default=23)
parser.add_argument('--n_estimators', type=int, required=False, default=1)
parser.add_argument('--learning_rate', type=float, required=False, default=0.7)
parser.add_argument('--booster', type=str, required=False, default="dart")
parser.add_argument('--seed', type=int, required=False, help='seed for seeding the PRNGs')

log = logging.getLogger(__name__)


def main(argv):

    args = parser.parse_args()

    run = wandb.init(project="framed", job_type="train_xgboost", tags=["train","xgboost"])
    run.config.update(args)

    # Seed PRNGs
    if not run.config.seed:
        run.config.update(
            dict(seed=random.randrange(2**16)), allow_val_change=True)
    log.info("Using PRNG seed: %s", run.config.seed)
    os.environ['PYTHONHASHSEED'] = str(run.config.seed)
    random.seed(run.config.seed)
    np.random.seed(run.config.seed)

    X_train, y_train = mf.load_datasets(run, args.trainsets.split(","), run.config.seed)
    X_test, y_test = mf.load_datasets(run, args.testsets.split(","), run.config.seed)

    ################# XGBoost #################
    log.info("Initializing XGBoost Classifier...")
    clf = xgboost.XGBClassifier(
        n_estimators=run.config.n_estimators,
        max_depth=run.config.max_depth,
        learning_rate=run.config.learning_rate,
        booster=run.config.booster,
        n_jobs=4,
        random_state=run.config.seed
    )

    log.info("Fitting model... ")
    clf.fit(X=X_train, y=y_train, sample_weight=None, callbacks=[wandb.xgboost.wandb_callback()])

    log.info("Predicting on trainset...")
    y_train_pred = clf.predict(X_train)
    wandb.log({"train.accuracy": sklearn.metrics.accuracy_score(y_train, y_train_pred)})
    wandb.log({"train": sklearn.metrics.classification_report(
        y_train, y_train_pred, labels=list(mc.classnames.keys()), target_names=mc.classnames.values(), output_dict=True)})

    log.info("Generating confusion matrix...")
    #wandb.log({"train.conf_mat_sklearn": wandb.sklearn.plot_confusion_matrix(y_train, y_train_pred)})
    wandb.log({"train_conf_mat": wandb.plot.confusion_matrix(
        preds=y_train_pred, y_true=y_train, class_names=list(mc.classnames.values()))})

    log.info("Predicting on testset...")
    y_test_pred = clf.predict(X_test)
    wandb.log({"test.accuracy": sklearn.metrics.accuracy_score(y_test, y_test_pred)})
    wandb.log({"test": sklearn.metrics.classification_report(
        y_test, y_test_pred, labels=list(mc.classnames.keys()), target_names=mc.classnames.values(), output_dict=True)})

    log.info("Generating confusion matrix...")
    wandb.sklearn.plot_confusion_matrix(y_test, y_test_pred)
    wandb.log({"test_conf_mat": wandb.plot.confusion_matrix(
        preds=y_test_pred,
        y_true=y_test,
        class_names=list(mc.classnames.values())
    )})

    artifact = wandb.Artifact(name='xgboost-classifier', type='xgboost-classifier')
    dump(clf, os.path.join(wandb.run.dir, "classifier.joblib"), compress=True)
    artifact.add_file(os.path.join(wandb.run.dir, "classifier.joblib"))
    run.log_artifact(artifact)


if __name__ == "__main__":
    main(sys.argv)
