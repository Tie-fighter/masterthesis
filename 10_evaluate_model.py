import argparse
import logging
import os
import random
import sys
import json

import numpy as np
import pandas as pd
import sklearn.metrics
import tensorflow as tf
from joblib import load
from sklearn.inspection import permutation_importance

import my_config as mc
import my_functions as mf
import wandb

parser = argparse.ArgumentParser()
parser.add_argument('--testsets', type=str, required=True,
                    help='the wandb datasets used for testing, comma-separated  (e.g. IDS2018-test:latest,myflows-test:latest)')
parser.add_argument('--model', type=str, required=True, help='the model to be evaluated e.g. dnn:v153')
parser.add_argument('--usebestmodel', type=bool, required=True, help='Use the best-model.h5 or the model.h5 ?')
parser.add_argument('--modeltype', type=str, required=True, help='adaboost-classifier, xgboost-classifier or dnn')
parser.add_argument('--seed', type=int, required=False, help='seed for seeding the PRNGs')

log = logging.getLogger(__name__)


def main(argv):
    args = parser.parse_args()

    run = wandb.init(project="framed", job_type="evaluate_model", tags=["postprocessing"])
    run.config.update(args)

    # Seed PRNGs
    if not run.config.seed:
        run.config.update(
            dict(seed=random.randrange(2**16)), allow_val_change=True)
    log.info("Using PRNG seed: %s", run.config.seed)
    os.environ['PYTHONHASHSEED'] = str(run.config.seed)
    random.seed(run.config.seed)
    np.random.seed(run.config.seed)
    tf.random.set_seed(run.config.seed)

    ### Code for downloading skipped since we are running everything on one machine

    # Load the datasets
    X_test, y_test = mf.load_datasets(run, args.testsets.split(","), run.config.seed)
    # "Converts a class vector (integers) to binary class matrix."
    y_test_onehot = tf.keras.utils.to_categorical(y_test)

    artifact = run.use_artifact('tie-fighter/framed/'+run.config.model, type=run.config.modeltype)
    artifact_dir = artifact.download()

    # define a custom scorer to pass onto permutation_importance, should only return the accuracy
    def my_scorer(self, *args,**kargs):
        acc = model.evaluate(x=args[0], y=args[1], return_dict=False)
        return acc[1]

    ################# DLNN #################
    if run.config.modeltype == "dnn":
        if run.config.usebestmodel == True:
            log.info("Loading best-model.h5 ...")
            model = tf.keras.models.load_model(os.path.join(artifact_dir, "model-best.h5"))
        else:
            log.info("Loading model.h5 ...")
            model = tf.keras.models.load_model(os.path.join(artifact_dir, "model.h5"))

        #predict with shuffled features to estimate feature importance
        log.info("Estimating feature importance...")
        log.info("This will require %s calls of model.evaluate()", 5*len(X_test.columns))
        result = permutation_importance(model, X_test, y_test_onehot, scoring=my_scorer)

    ################# AdaBoost & XGBoost #################
    elif run.config.modeltype == "adaboost-classifier" or run.config.modeltype == "xgboost-classifier":
        log.info("Loading classifier.joblib ...")
        clf = load(os.path.join(artifact_dir, "classifier.joblib"))

        # predict with shuffled features to estimate feature importance
        log.info("Estimating feature importance...")
        log.info("This will require %s calls of sklearn.metrics.accuracy_score()", 5*len(X_test.columns))
        result = permutation_importance(clf, X_test, y_test, scoring='accuracy', n_jobs=-1)

    else:
        print("modeltype parameter did not have expected value")
        exit()

    fi_data = list()
    for c in X_test.loc[:, X_test.columns != 'Label'].columns: # extract column names and save metrics
        fi_data.append(
            [
                c,
                [
                result["importances"][X_test.columns.get_loc(c)][0],
                result["importances"][X_test.columns.get_loc(c)][1],
                result["importances"][X_test.columns.get_loc(c)][2],
                result["importances"][X_test.columns.get_loc(c)][3],
                result["importances"][X_test.columns.get_loc(c)][4]],
            result["importances_mean"][X_test.columns.get_loc(c)],
            result["importances_std"][X_test.columns.get_loc(c)]])

    wandb.log({"featureimportance": wandb.Table(data=fi_data,
                                columns = ["feature", "values", "mean", "stddev"])})


if __name__ == "__main__":
    main(sys.argv)