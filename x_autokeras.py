import logging

import autokeras as ak
import pandas
from wandb.keras import WandbCallback

import my_config as mc
import wandb

labels = dict()


def main():
    log = logging.getLogger(__name__)

    # disable output truncating
    #pandas.set_option('display.max_rows', None)

    trainset = pandas.DataFrame()
    log.info("Reading: %smy_3_train.csv", mc.workingdir)
    trainset = pandas.read_csv("%smy_3_train.csv", mc.workingdir)  # , dtype=dtypes)

    testset = pandas.DataFrame()
    log.info("Reading: %smy_3_test.csv", mc.workingdir)
    testset = pandas.read_csv("%smy_3_test.csv", mc.workingdir)  # , dtype=dtypes)

    X_train = trainset.loc[:, trainset.columns != 'Label']
    X_test = testset.loc[:, trainset.columns != 'Label']
    y_train = trainset.loc[:, trainset.columns == 'Label']
    y_test = testset.loc[:, trainset.columns == 'Label']

    log.info("Initializing wandb...")
    wandb.init(project="framed", project="autokeras")

    # Initialize the structured data classifier.
    log.info("Initializing classifier...")
    clf = ak.StructuredDataClassifier(directory=mc.workingdir + "autokeras")
    # Feed the structured data classifier with training data.
    log.info("Starting fit...")
    clf.fit(x=X_train, y=y_train, validation_data=(
        X_test, y_test), callbacks=[WandbCallback()])

    # Predict with the best model.
    #predicted_y = clf.predict(X_test)
    # Evaluate the best model with testing data.
    print(clf.evaluate(x=X_test, y=y_test))

    # Save model to wandb
    # model.save(os.path.join(wandb.run.dir, "model.h5"))


if __name__ == "__main__":
    main()
