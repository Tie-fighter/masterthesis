import argparse
import logging
import os
import random
import sys
import time

import numpy as np
import sklearn.metrics
import tensorflow as tf
from sklearn.utils.class_weight import compute_class_weight
from wandb.keras import WandbCallback

import my_config as mc
import my_functions as mf
import wandb

parser = argparse.ArgumentParser()
parser.add_argument('--trainsets', type=str, required=True,
                    help='the wandb datasets used for training, comma-separated (e.g. IDS2018-train:latest,myflows-train:latest)')
parser.add_argument('--testsets', type=str, required=True,
                    help='the wandb datasets used for testing, comma-separated  (e.g. IDS2018-test:latest,myflows-test:latest)')
parser.add_argument('--batch_size', type=int, required=False, default=1024)
parser.add_argument('--initialization', type=str, required=False, default="uniform")
parser.add_argument('--hidden_layers', type=int, required=False, default=1)
parser.add_argument('--layer_1', type=int, required=False, default=100)
parser.add_argument('--layer_2', type=int, required=False, default=100)
parser.add_argument('--layer_3', type=int, required=False, default=100)
parser.add_argument('--layer_4', type=int, required=False, default=100)
parser.add_argument('--layer_5', type=int, required=False, default=100)
parser.add_argument('--learning_rate', type=float, required=False, default=0.00001)
parser.add_argument('--batch_normalization', type=bool, required=False, default=True)
parser.add_argument('--epochs', type=int, required=False, default=300)
parser.add_argument('--optimizer', type=str, required=False, default="adamax")
parser.add_argument('--activation', type=str, required=False, default="relu")
parser.add_argument('--seed', type=int, required=False, help='seed for seeding the PRNGs')

log = logging.getLogger(__name__)


def main(argv):

    args = parser.parse_args()

    run = wandb.init(project="framed", job_type="train_dnn", tags=["train", "dnn"])
    run.config.update(args)

    # Seed PRNGs
    if not run.config.seed:
        run.config.update(
            dict(seed=random.randrange(2**16)), allow_val_change=True)
    log.info("Using PRNG seed: %s", run.config.seed)
    os.environ['PYTHONHASHSEED'] = str(run.config.seed)
    random.seed(run.config.seed)
    np.random.seed(run.config.seed)
    tf.random.set_seed(run.config.seed)

    X_train, y_train = mf.load_datasets(run, args.trainsets.split(","), run.config.seed)
    X_test, y_test = mf.load_datasets(run, args.testsets.split(","), run.config.seed)

    # we need these numbers to construct the dnn
    num_features = len(X_train.columns)
    num_classes = len(np.unique(y_train))

    # calculate class weights
    class_weight_list = compute_class_weight(
        class_weight='balanced', classes=np.unique(y_train), y=y_train)
    my_class_weights = dict(zip(np.unique(y_train), class_weight_list))
    for i in range(num_classes):
        my_class_weights.setdefault(i, 0)
    log.info("Using these class weights: ")
    log.info(my_class_weights)

    # "Converts a class vector (integers) to binary class matrix."
    y_train_onehot = tf.keras.utils.to_categorical(y_train)
    y_test_onehot = tf.keras.utils.to_categorical(y_test)

    model_artifact = wandb.Artifact(
        "dnn", type="dnn",
        metadata=dict(run.config))

    ################# DLNN #################
    # Model Architecture
    model = tf.keras.Sequential()

    # Batch Normalization layer
    if run.config.batch_normalization:
        model.add(tf.keras.layers.BatchNormalization())

    # Hidden layers:
    model.add(tf.keras.layers.Dense(run.config.layer_1,
                                    activation=run.config.activation,
                                    kernel_initializer=run.config.initialization
                                    ))
    if run.config.hidden_layers >= 2:
        model.add(tf.keras.layers.Dense(run.config.layer_2,
                                        activation=run.config.activation,
                                        kernel_initializer=run.config.initialization
                                        ))

    if run.config.hidden_layers >= 3:
        model.add(tf.keras.layers.Dense(run.config.layer_3,
                                        activation=run.config.activation,
                                        kernel_initializer=run.config.initialization
                                        ))
    if run.config.hidden_layers >= 4:
        model.add(tf.keras.layers.Dense(run.config.layer_4,
                                      activation=run.config.activation,
                                      kernel_initializer=run.config.initialization
                                      ))
    if run.config.hidden_layers >= 5:
        model.add(tf.keras.layers.Dense(run.config.layer_5,
                                        activation=run.config.activation,
                                        kernel_initializer=run.config.initialization
                                        ))
    # Output layer: Softmax
    model.add(tf.keras.layers.Dense(len(my_class_weights),
                                    activation="softmax"
                                    ))

    model.compile(
        optimizer=tf.keras.optimizers.get({"class_name": run.config.optimizer,
                               "config": {"learning_rate": run.config.learning_rate}}),
        loss=tf.keras.losses.CategoricalCrossentropy(),
        metrics=tf.keras.metrics.CategoricalAccuracy(),
    )

    time_callback = mf.TimeHistory()

    log.info("Fitting model...")
    model.fit(
        x=X_train, y=y_train_onehot,
        batch_size=run.config.batch_size,
        epochs=run.config.epochs,
        class_weight=my_class_weights,
        verbose=2,
        callbacks=[WandbCallback(), time_callback],
        validation_data=(X_test, y_test_onehot)
    )

    # Save model to wandb
    model.save(os.path.join(wandb.run.dir, "model.h5"))
    model_artifact.add_file(os.path.join(wandb.run.dir, "model.h5"))
    while not os.path.exists(os.path.join(wandb.run.dir, "model.h5")):
        time.sleep(1)
    model_artifact.add_file(os.path.join(wandb.run.dir, "model-best.h5"))
    run.log_artifact(model_artifact)

    log.info("Predicting on trainset...")
    y_train_pred = model.predict(X_train)
    y_train_pred = np.argmax(y_train_pred, axis=1)
    wandb.log({"train.accuracy": sklearn.metrics.accuracy_score(y_train, y_train_pred)})
    wandb.log({"train": sklearn.metrics.classification_report(
        y_train, y_train_pred, labels=list(mc.classnames.keys()), target_names=mc.classnames.values(), output_dict=True)})

    log.info("Generating confusion matrix...")
    #wandb.log({"train.conf_mat_sklearn": wandb.sklearn.plot_confusion_matrix(y_train, y_train_pred)})
    wandb.log({"train.conf_mat_wandb": wandb.plot.confusion_matrix(
        preds=y_train_pred, y_true=y_train, class_names=list(mc.classnames.values()))})

    log.info("Predicting on testset...")
    y_test_pred = model.predict(X_test)
    y_test_pred = np.argmax(y_test_pred, axis=1)
    wandb.log({"test.accuracy": sklearn.metrics.accuracy_score(y_test, y_test_pred)})
    wandb.log({"test": sklearn.metrics.classification_report(
        y_test, y_test_pred, labels=list(mc.classnames.keys()), target_names=mc.classnames.values(), output_dict=True)})

    log.info("Generating confusion matrix...")
    wandb.sklearn.plot_confusion_matrix(y_test, y_test_pred)
    wandb.log({"test.conf_mat_wandb": wandb.plot.confusion_matrix(
        preds=y_test_pred, y_true=y_test, class_names=list(mc.classnames.values()))})


if __name__ == "__main__":
    main(sys.argv)
