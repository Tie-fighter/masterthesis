import logging

import numpy as np
import pandas
import sklearn.metrics
from sklearn.naive_bayes import ComplementNB

import my_config as mc


def main():
    log = logging.getLogger(__name__)

    # disable output truncating
    #pandas.set_option('display.max_rows', None)

    trainset = pandas.DataFrame()
    log.info("Reading: %smy_3_train.csv", mc.workingdir)
    # , dtype = dtypes)
    trainset = pandas.read_csv("%smy_3_train.csv", mc.workingdir)
    # infer best possible dtypes
    trainset = trainset.convert_dtypes()

    testset = pandas.DataFrame()
    log.info("Reading: %smy_3_test.csv", mc.workingdir)
    # , dtype = dtypes)
    testset = pandas.read_csv("%smy_3_test.csv", mc.workingdir)
    testset = testset.convert_dtypes()

    clf = ComplementNB()

    print("trainset.loc[:, trainset.columns != 'Label']")
    print(trainset.loc[:, trainset.columns != 'Label'])  # (X)
    print("trainset.loc[:, trainset.columns == 'Label']")
    print(trainset.loc[:, trainset.columns == 'Label'])  # (y)

    X_train = trainset.loc[:, trainset.columns != 'Label']
    y_train = trainset.loc[:, trainset.columns == 'Label']

    y_train = np.ravel(y_train)
    y_train = y_train.astype('int')

    X_test = testset.loc[:, trainset.columns != 'Label']
    y_test = testset.loc[:, trainset.columns == 'Label']

    y_test = np.ravel(y_test)
    y_test = y_test.astype('int')

    # Train with train set
    log.info("Fitting Complement Naive Bayes...")
    clf.fit(X_train, y_train)

    # Test against test set
    log.info("Predicting test dataset")
    y_predicted = clf.predict(X_test)  # (X)

    # Calculate scores
    precision = sklearn.metrics.precision_score(y_test, y_predicted)
    recall = sklearn.metrics.recall_score(y_test, y_predicted)
    f1 = sklearn.metrics.f1_score(y_test, y_predicted, average='micro')
    log.info("Precision: %s", precision)
    log.info("Recall: %s", recall)
    log.info("F1 score: %s", f1)


if __name__ == "__main__":
    main()
