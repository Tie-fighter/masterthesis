import argparse
import logging
import os
import random
import sys

import numpy as np
import sklearn
import sklearn.ensemble
import sklearn.metrics
import sklearn.tree
from joblib import dump

import my_functions as mf
import my_config as mc
import wandb

parser = argparse.ArgumentParser()
parser.add_argument('--trainsets', type=str, required=True,
                    help='the wandb datasets used for training, comma-separated (e.g. IDS2018-train:latest,myflows-train:latest)')
parser.add_argument('--testsets', type=str, required=True,
                    help='the wandb datasets used for training, comma-separated  (e.g. IDS2018-test:latest,myflows-test:latest)')
parser.add_argument('--base-estimator', type=str, required=False,
                    default="DecisionTreeClassifier(max_depth=1)")
parser.add_argument('--max_depth', type=int, required=False, default=9)
parser.add_argument('--n_estimators', type=int, required=False, default=1)
parser.add_argument('--learning_rate', type=float, required=False, default=0.05)
parser.add_argument('--algorithm', type=str, required=False, default="SAMME.R")
parser.add_argument('--seed', type=int, required=False, help='seed for seeding the PRNGs')

log = logging.getLogger(__name__)


def main(argv):

    args = parser.parse_args()

    run = wandb.init(project="framed", job_type="train_adaboost", tags=["train", "adaboost"])
    run.config.update(args)

    # Seed PRNGs
    if not run.config.seed:
        run.config.update(
            dict(seed=random.randrange(2**16)), allow_val_change=True)

    log.info("Using PRNG seed: %s", run.config.seed)
    os.environ['PYTHONHASHSEED'] = str(run.config.seed)
    random.seed(run.config.seed)
    np.random.seed(run.config.seed)

    X_train, y_train = mf.load_datasets(run, args.trainsets.split(","), run.config.seed)
    X_test, y_test = mf.load_datasets(run, args.testsets.split(","), run.config.seed)

    ################# Adaboost #################
    log.info("Initializing Adaboost Classifier...")
    clf = sklearn.ensemble.AdaBoostClassifier(
        base_estimator=sklearn.tree.DecisionTreeClassifier(max_depth=run.config.max_depth),
        n_estimators=run.config.n_estimators,
        learning_rate=run.config.learning_rate,
        algorithm=run.config.algorithm,
        random_state=run.config.seed
    )

    log.info("Fitting model... ")
    clf.fit(X=X_train, y=y_train, sample_weight=None)

    log.info("Predicting on trainset...")
    y_train_pred = clf.predict(X_train)
    wandb.log({"train.accuracy": sklearn.metrics.accuracy_score(y_train, y_train_pred)})
    wandb.log({"train": sklearn.metrics.classification_report(
        y_train, y_train_pred, labels=list(mc.classnames.keys()), target_names=mc.classnames.values(), output_dict=True)})

    log.info("Generating confusion matrix...")
    #wandb.log({"train.conf_mat_sklearn": wandb.sklearn.plot_confusion_matrix(y_train, y_train_pred)})
    wandb.log({"conf_mat": wandb.plot.confusion_matrix(
        preds=y_train_pred, y_true=y_train, class_names=list(mc.classnames.values()))})

    log.info("Predicting on testset...")
    y_test_pred = clf.predict(X_test)
    wandb.log({"test.accuracy": sklearn.metrics.accuracy_score(y_test, y_test_pred)})
    wandb.log({"test": sklearn.metrics.classification_report(
        y_test, y_test_pred, labels=list(mc.classnames.keys()), target_names=mc.classnames.values(), output_dict=True)})

    log.info("Generating confusion matrix...")
    wandb.sklearn.plot_confusion_matrix(y_test, y_test_pred)
    wandb.log({"test_conf_mat": wandb.plot.confusion_matrix(
        preds=y_test_pred, y_true=y_test, class_names=list(mc.classnames.values()))})

    artifact = wandb.Artifact(name='adaboost-classifier', type='adaboost-classifier')
    dump(clf, os.path.join(wandb.run.dir, "classifier.joblib"), compress=True)
    artifact.add_file(os.path.join(wandb.run.dir, "classifier.joblib"))
    run.log_artifact(artifact)


if __name__ == "__main__":
    main(sys.argv)
